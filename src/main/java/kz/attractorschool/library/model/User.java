package kz.attractorschool.library.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Document(collection = "users")
@Data
public class User {
    @Id
    private String email;
    private String name;
    private String surname;
    private String phoneNumber;
    @DBRef
    @Field("books")
    private List<Book> books = new ArrayList<>();

    public User(String email, String name, String surname, String phoneNumber) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.phoneNumber = phoneNumber;
    }
}
