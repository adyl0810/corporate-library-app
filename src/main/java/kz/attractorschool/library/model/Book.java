package kz.attractorschool.library.model;

import kz.attractorschool.library.service.Status;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@Document(collection = "books")
@Data
public class Book {
    @Id
    private String id;
    private String name;
    private String author;
    private String photo;
    private Integer yearPublished;
    private String description;
    private LocalDate dateAdded;
    private Status status;
    private Category category;


    public Book(String name, String author, Category category) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.author = author;
        this.category = category;
        this.status = Status.AVAILABLE;
        this.dateAdded = LocalDate.now();
    }
}
