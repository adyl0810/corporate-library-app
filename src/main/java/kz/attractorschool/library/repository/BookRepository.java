package kz.attractorschool.library.repository;

import kz.attractorschool.library.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookRepository extends MongoRepository<Book, String> {
}
