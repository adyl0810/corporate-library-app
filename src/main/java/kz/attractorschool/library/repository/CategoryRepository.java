package kz.attractorschool.library.repository;

import kz.attractorschool.library.model.Category;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface CategoryRepository extends MongoRepository<Category, String> {
    Category findByName(String name);
}
