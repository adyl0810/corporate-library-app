package kz.attractorschool.library.controller;

import kz.attractorschool.library.model.Book;
import kz.attractorschool.library.model.Category;
import kz.attractorschool.library.model.User;
import kz.attractorschool.library.repository.BookRepository;
import kz.attractorschool.library.repository.CategoryRepository;
import kz.attractorschool.library.repository.UserRepository;
import kz.attractorschool.library.service.BookService;
import kz.attractorschool.library.service.CategoryService;
import kz.attractorschool.library.service.Status;
import kz.attractorschool.library.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Controller
public class BooksController {
    private final BookRepository bookRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final BookService bookService;
    private final UserService userService;
    private final CategoryService categoryService;

    public BooksController(BookRepository bookRepository, UserRepository userRepository, CategoryRepository categoryRepository, BookService bookService, UserService userService, CategoryService categoryService) {
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.bookService = bookService;
        this.userService = userService;
        this.categoryService = categoryService;
    }

    @GetMapping
    public String get(Model model,
                      @RequestParam(defaultValue = "1") int page) {
        int pageSize = 2;
        int currentPage = page - 1;
        if (currentPage < 0)
            currentPage = 0;
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<Book> bookPage = bookRepository.findAll(pageable);
        List<Book> books = bookPage.toList();
        boolean hasNext = !books.isEmpty();
        model.addAttribute("currentPage", (currentPage+1));
        model.addAttribute("hasPrevious", pageable.hasPrevious());
        model.addAttribute("hasNext", hasNext);
        model.addAttribute("books", books);
        return "books";
    }

    @GetMapping("/book/{id}")
    public String getBook(@PathVariable String id,
                          Model model) {
        Book book = bookService.getById(id);
        model.addAttribute("book", book);
        return "book";
    }

    @GetMapping("/add-book")
    public String getAdd(Model model) {
        List<Category> categories = categoryRepository.findAll();
        model.addAttribute("categories", categories);
        return "add-book";
    }

    @GetMapping("book/{id}/edit-book")
    public String getEdit(@PathVariable String id, Model model) {
        Book book = bookService.getById(id);
        model.addAttribute(book);
        return "edit-book";
    }

    @GetMapping("book/{id}/delete-book")
    public String getDelete(@PathVariable String id, Model model) {
        Book book = bookService.getById(id);
        model.addAttribute(book);
        return "delete-book";
    }

    @GetMapping("book/{id}/take-book")
    public String getTake(@PathVariable String id, Model model) {
        Book book = bookService.getById(id);
        List<Book> books = bookRepository.findAll();
        model.addAttribute(book);
        model.addAttribute(books);
        return "take-book";
    }

    @GetMapping("add-book/add-category")
    public String getCategory(Model model) {
        List<Category> categories = categoryRepository.findAll();
        model.addAttribute("categories", categories);
        return "add-category";
    }

    @PostMapping("/add-book")
    public String addBook(@RequestParam String name,
                          @RequestParam String author,
                          @RequestParam String categoryName,
                          @RequestParam("photo") MultipartFile multipartFile,
                          @RequestParam(required = false) Integer year,
                          @RequestParam(required = false) String description) throws IOException {
        Category category = categoryRepository.findByName(categoryName);
        bookService.addBook(name, author, category, multipartFile, year, description);
        return "redirect:/";
    }


    @PostMapping("book/{id}/edit-book")
    public String editBook(@PathVariable String id,
                           @RequestParam String name,
                           @RequestParam String author,
                           @RequestParam(value = "photo", required = false) MultipartFile multipartFile,
                           @RequestParam(required = false) Integer year,
                           @RequestParam String description) throws IOException {
        bookService.editBook(id, name, author, multipartFile, year, description);
        return "redirect:/";
    }

    @PostMapping("book/{id}/delete-book")
    public String deleteBook(@PathVariable String id) {
        Book book = bookService.getById(id);
        bookRepository.delete(book);
        return "redirect:/";
    }

    @PostMapping("book/{id}/take-book")
    public String takeBook(@PathVariable String id,
                           @RequestParam String email) {
        User user = userService.getUserByEmail(email);
        List<Book> userBooks = user.getBooks();
        Book book = bookRepository.findById(id).orElse(null);
        userBooks.add(book);
        book.setStatus(Status.TAKEN);
        user.setBooks(userBooks);
        bookRepository.save(book);
        userRepository.save(user);
        return "redirect:/account/" + email;
    }

    @PostMapping("add-book/add-category")
    public String addCategory(@RequestParam String name) {
        categoryService.addCategory(name);
        return "redirect:/add-book/add-category";
    }
}
