package kz.attractorschool.library.controller;

import kz.attractorschool.library.model.Book;
import kz.attractorschool.library.model.User;
import kz.attractorschool.library.repository.BookRepository;
import kz.attractorschool.library.repository.UserRepository;
import kz.attractorschool.library.service.BookService;
import kz.attractorschool.library.service.Status;
import kz.attractorschool.library.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class UserController {
    private final UserRepository userRepository;
    private final UserService userService;
    private final BookService bookService;
    private final BookRepository bookRepository;

    public UserController(UserRepository userRepository, UserService userService, BookService bookService, BookRepository bookRepository) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.bookService = bookService;
        this.bookRepository = bookRepository;
    }

    @GetMapping("/account")
    public String get(Model model) {
        List<User> users = userRepository.findAll();
        model.addAttribute(users);
        return "account";
    }

    @GetMapping("/account/register")
    public String getRegister() {
        return "register";
    }

    @GetMapping("/account/{email}")
    public String getUser(@PathVariable String email,
                          Model model) {
        User user = userService.getUserByEmail(email);
        model.addAttribute("user", user);
        return "user";
    }

    @PostMapping("/account")
    public String login(@RequestParam String email) {
        User user = userService.getUserByEmail(email);
        return "redirect:/account/" + email;
    }

    @PostMapping("/account/{email}")
    public String returnBook(@PathVariable String email,
                          @RequestParam String bookId) {
        User user = userService.getUserByEmail(email);
        List<Book> userBooks = user.getBooks();
        Book book = bookRepository.findById(bookId).orElse(null);
        userBooks.remove(book);
        book.setStatus(Status.AVAILABLE);
        user.setBooks(userBooks);
        bookRepository.save(book);
        userRepository.save(user);
        return "redirect:/account/{email}";
    }

    @PostMapping("/account/register")
    public String register(@RequestParam String email,
                           @RequestParam String name,
                           @RequestParam String surname,
                           @RequestParam String phoneNumber) {
        userService.addUser(email, name, surname, phoneNumber);
        return "redirect:/";
    }
}
