package kz.attractorschool.library.service;

import kz.attractorschool.library.model.Book;
import kz.attractorschool.library.model.Category;
import kz.attractorschool.library.repository.BookRepository;
import kz.attractorschool.library.util.FileUploadUtil;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@Service
public class BookService {
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book getById(String id) {
        return bookRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Книга с id %s не найдена", id)
                ));
    }

    public void addBook(String name, String author, Category category, MultipartFile multipartFile, Integer yearPublished, String description) throws IOException {
        Book book = new Book(name, author, category);
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        book.setPhoto(fileName);
        String uploadDir = "images/" + book.getName();
        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
        if (yearPublished != null)
            book.setYearPublished(yearPublished);
        if (description != null)
            book.setDescription(description);
        bookRepository.save(book);
    }

    public void editBook(String id, String name, String author, MultipartFile multipartFile, Integer yearPublished, String description) throws IOException {
        Book book = bookRepository.findById(id).orElse(null);
        if (name != null)
            book.setName(name);
        if (author != null)
            book.setAuthor(author);
        if (multipartFile != null) {
            String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
            book.setPhoto(fileName);
            String uploadDir = "images/" + book.getName();
            FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
        }
        if (yearPublished != null)
            book.setYearPublished(yearPublished);
        if (description != null)
            book.setDescription(description);
        bookRepository.save(book);

    }
}
