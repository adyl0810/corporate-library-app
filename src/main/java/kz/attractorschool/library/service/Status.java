package kz.attractorschool.library.service;

public enum Status {

    AVAILABLE("Доступна"),
    TAKEN("Нет в наличии");


    private final String name;

    Status(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
