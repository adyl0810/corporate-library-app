package kz.attractorschool.library.service;

import kz.attractorschool.library.model.Category;
import kz.attractorschool.library.repository.CategoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }
    public Category getById(String id) {
        return categoryRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Категория с id %s не найдена", id)
                ));
    }

    public void addCategory(String name) {
        Category category = new Category(name);
        categoryRepository.save(category);
    }

}
