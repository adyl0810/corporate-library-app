package kz.attractorschool.library.service;

import kz.attractorschool.library.model.Book;
import kz.attractorschool.library.model.User;
import kz.attractorschool.library.repository.BookRepository;
import kz.attractorschool.library.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class UserService {
    private UserRepository userRepository;
    private BookRepository bookRepository;
    private BookService bookService;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserByEmail(String email) {
        return userRepository.findById(email).orElseThrow(() ->
                new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("Пользователь с email-ом %s не найден", email)
                ));
    }

    public void addUser(String email, String name, String surname, String phoneNumber) {
        User user = new User(email, name, surname, phoneNumber);
        userRepository.save(user);
    }

    public void takeBook(String id, String email) {
        User user = getUserByEmail(email);
        if (user.getBooks().size() < 3) {
            List<Book> userBooks = user.getBooks();
            Book book = bookRepository.findById(id).orElse(null);
            userBooks.add(book);
            user.setBooks(userBooks);
            userRepository.save(user);
        }
    }
}
